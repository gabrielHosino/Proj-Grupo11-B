#include <cuda.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <opencv/highgui.h>
#include <opencv/cxcore.h>
#include <opencv/cv.h>

//nvcc smoothcudacolor.cu -o teste `pkg-config --libs --cflags opencv` -ldl
// ./teste <nome da imagem>

__global__ void smooth(int *image,int *image_smooth, int cols, int rows);

int timespec_subtract(struct timespec *result, struct timespec *t2, struct timespec *t1)
{
  long int diff = (t2->tv_nsec + 1000000000L * t2->tv_sec) - (t1->tv_nsec + 1000000000L * t1->tv_sec);
  result->tv_sec = diff / 1000000000L;
  result->tv_nsec = diff % 1000000000L;

  return (diff<0);
}


int main(int argc, char** argv){
	CvMat *imageIn;
	int *image_h_r,*image_h_g,*image_h_b, *image_d;
	int *imageout_h_r,*imageout_h_g,*imageout_h_b, *imageout_d;
	int i, j;
	int size;
  CvScalar rgb;
  int parametersOut[2];
  struct timespec t_begin, t_end, t_diff;

	//abre a imagem
	imageIn = cvLoadImageM( argv[1], CV_LOAD_IMAGE_GRAYSCALE);
	if (!imageIn)
        {
            printf("No image data \n");
            return -1;
        }

  //variavel auxiliar com o tamanho dos vetores
  size = imageIn->rows * imageIn->cols;
    
  //cria o vetor auxiliar que vai ter os valores de cada pixel
  image_h_b = (int*) malloc(size*sizeof(int));
  image_h_g = (int*) malloc(size*sizeof(int));
  image_h_r = (int*) malloc(size*sizeof(int));
  imageout_h_b = (int*) malloc(size*sizeof(int));
  imageout_h_g = (int*) malloc(size*sizeof(int));
  imageout_h_r = (int*) malloc(size*sizeof(int));
    
  //copia os valores da imagem de entrada para o vetor auxiliar
  for(i = 0; i < imageIn->rows;i++){
  	for(j = 0; j < imageIn->cols; j++){rgb = cvGet2D(imageIn, i, j);
        image_h_b[i*imageIn->cols + j] = (int)rgb.val[0];
        image_h_g[i*imageIn->cols + j] = (int)rgb.val[1];
        image_h_r[i*imageIn->cols + j] = (int)rgb.val[2];

    }
  }
    
  //aloca memoria na placa de video
  cudaMalloc(&image_d, size);
  cudaMalloc(&imageout_d, size);

  //pega tempo
  clock_gettime(CLOCK_REALTIME, &t_begin);

  //transfere a imagem que deve ser tratada para a placa de video
  cudaMemcpy(image_d, image_h_b, size, cudaMemcpyHostToDevice);

  //chama kernel e realiza smooth (criar funçao)
  smooth<<<ceil(size/ 256 + 1), 256>>>(image_d, imageout_d,imageIn->cols, imageIn->rows);

  //transfere o resultado do smooth de volta para o host
  cudaMemcpy(imageout_h_b, imageout_d, size, cudaMemcpyDeviceToHost);



  //transfere a imagem que deve ser tratada para a placa de video
  cudaMemcpy(image_d, image_h_g, size, cudaMemcpyHostToDevice);

  //chama kernel e realiza smooth (criar funçao)
  smooth<<<ceil(size/256 + 1), 256>>>(image_d, imageout_d,imageIn->cols, imageIn->rows);

  //transfere o resultado do smooth de volta para o host
  cudaMemcpy(imageout_h_g, imageout_d, size, cudaMemcpyDeviceToHost);



  //transfere a imagem que deve ser tratada para a placa de video
  cudaMemcpy(image_d, image_h_r, size, cudaMemcpyHostToDevice);

  //chama kernel e realiza smooth (criar funçao)
  smooth<<<ceil(size/ 256 + 1), 256>>>(image_d, imageout_d,imageIn->cols, imageIn->rows);

  //transfere o resultado do smooth de volta para o host
  cudaMemcpy(imageout_h_r, imageout_d, size, cudaMemcpyDeviceToHost);

  //pega tempo
  clock_gettime(CLOCK_REALTIME, &t_end);

  //passa os valores de volta ao padrao opencv
  for(i = 0; i < imageIn->rows;i++){
    for(j = 0; j < imageIn->cols; j++){
    	rgb.val[0] = (double)imageout_h_b[i*imageIn->cols +j];
      rgb.val[1] = (double)imageout_h_g[i*imageIn->cols +j];
      rgb.val[2] = (double)imageout_h_r[i*imageIn->cols +j];
                    
      cvSet2D(imageIn, i, j,rgb); 
    }
  }

  //Seta os parametros que serao usados na hora de salvar a nova imagem em disco
  parametersOut[0] = CV_IMWRITE_PNG_COMPRESSION;
  parametersOut[1] = 3;
        
  //Salva a nova imagem em disco
  cvSaveImage("out.jpg", imageIn, parametersOut);

  //calcula o tempo utilizado
  timespec_subtract(&t_diff, &t_end, &t_begin);
  fprintf(stderr, "%ld.%09ld segs\n", t_diff.tv_sec, t_diff.tv_nsec);

  // Libera memória do device e host
  cudaFree(image_d);
  cudaFree(imageout_d);
  free(image_h_r);
  free(image_h_g);
  free(image_h_b);
  free(imageout_h_r);
  free(imageout_h_g);
  free(imageout_h_b);

  return 0;
}

__global__ void smooth(int *image,int *image_smooth, int cols, int rows){

  int sum;
  int average;
  int num_pixels;
  int a,b;
  int row_px,col_px;

  int id = blockIdx.x * blockDim.x + threadIdx.x;

  row_px = id / cols;
  col_px = id % cols;

  sum = 0;
  num_pixels = 0;

  if(id < (rows*cols)){
    for(a = row_px - 2; a <= row_px + 2; a++){
      for(b = col_px -2; b <= col_px + 2; b++){
        if(a >= 0 && a < rows && b >= 0 && b < cols){
          sum = sum + image[a*cols + b];
          num_pixels++;
        }
      }
    }
    average = sum/num_pixels;
    image_smooth[row_px*cols + col_px] = average;
  }
  
}

